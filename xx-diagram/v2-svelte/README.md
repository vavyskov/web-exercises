# svelte-diagram

## Creating a project

1. Create project:
    ```bash
    npm create svelte@latest .
    npm install
    git init && git add -A && git commit -m "Initial commit"
    ```

1. Configure [static adapter](https://kit.svelte.dev/docs/adapter-static).

1. Add and configure Mermaid.js library:
    ```bash
    npm install --save-dev mermaid
    ```

## Developing

```bash
npm install
npm run dev
```
or
```bash
npm run dev -- --open
```

## Building

```bash
npm run build
npm run preview
```

## Notes

Links:
- [HTML5 entities](https://html.spec.whatwg.org/multipage/named-characters.html)
