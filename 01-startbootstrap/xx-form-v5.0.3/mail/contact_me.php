<?php
// Check for empty fields
if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone']) || empty($_POST['message']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  http_response_code(500);
  exit();
}

$name = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));

// Create the email and send the message
$to = "test@ethereal.email"; // Add your email address inbetween the "" replacing yourname@yourdomain.com - This is where the form will send a message to.



// Doplneni kodovani
$subject = "=?UTF-8?B?".base64_encode("Kontaktní formulář:  $name")."?=";



$body = "Obdrželi jste novou zprávu z kontaktního formuláře.\n\n"."Zde jsou podrobnosti:\n\nJméno: $name\n\nE-mail: $email\n\nTelefon: $phone\n\nZpráva:\n$message";



// Doplneni kodovani
$header = "MIME-Version: 1.0\n";
$header .= "Content-Type: text/plain; charset=utf-8\n";
$header .= "From: noreply@vavyskov.cz\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.



$header .= "Reply-To: $email";

if(!mail($to, $subject, $body, $header))
  http_response_code(500);
?>
