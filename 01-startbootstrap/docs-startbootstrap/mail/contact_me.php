<?php
// Configuration
$from = [
    'email' => 'user@vavyskov.cz',
    'name' => 'Jméno odesílatele',
    'password' => 'password',
];
$to = [
    'email' => 'foster.schiller5@ethereal.email', // Etc. https://ethereal.email/
    'name' => 'Jméno příjemce',
]; 

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load PHPMailer (install by composer)
// require 'vendor/autoload.php';

//Load PHPMailer (manual download)
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// Check for empty fields
if (
    empty($_POST['name']) 
    || empty($_POST['email']) 
    || empty($_POST['phone']) 
    || empty($_POST['message']) 
    || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)
) {
    // Internal Server Error (form hacking, direct access to the URL etc.)
    http_response_code(500);
    exit();
}

// Strip HTML tags and convert special characters
$name = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = nl2br($_POST['message']);

// Message
$subject = "Kontaktní formulář: $name";
$content_text = "Odesílatel:\n - jméno: $name\n - e-mail: $email\n - telefon: $phone\n\nZpráva:\n$message";
$content_html = "<p>Odesílatel:</p><ul><li>jméno: <b>$name</b></li><li>e-mail: <b>$email</b></li><li>telefon: <b>$phone</b></li></ul><p>Zpráva:<br><b>$message</b></p>";

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    // Character encoding
    $mail->CharSet = 'utf-8';

    // Localization
    $mail->setLanguage('cs', 'PHPMailer/language/');

    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_OFF;                         //Enable verbose debug output (DEBUG_OFF, DEBUG_CLIENT, DEBUG_SERVER, DEBUG_CONNECTION, DEBUG_LOWLEVEL)
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.vavyskov.cz';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $from['email'];                         //SMTP username
    $mail->Password   = $from['password'];                      //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable implicit TLS encryption
    $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom($from['email'], $from['name']);
    $mail->addAddress($to['email'], $to['name']);               //Add a recipient, name is optional
    //$mail->addAddress('ellen@example.com');                   //Add another a recipient, name is optional
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');            //Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');       //Optional name

    //Content
    $mail->isHTML(true);                                        //Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $content_html;
    $mail->AltBody = strip_tags($content_text);

    $mail->send();

    //echo 'Message has been sent.';

    // Success
    readfile('success.html');
} catch (Exception $e) {
    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    //echo $e->errorMessage();

    // Failure
    readfile('failure.html');
}

// Global exception
/* catch (\Exception $e)
{
    // PHP exception (note the backslash to select the global namespace Exception class).
    echo $e->getMessage();
} */