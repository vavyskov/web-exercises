// Pomocna promenna
let vypocet = false;

// Prirazeni objektu promenne
let hodnota = document.getElementById("hodnota");

// Smazani vstupu
function smazVse() {
    hodnota.value = "0";
}

// Vypocet
document.getElementById("vypocitej").addEventListener("click", function () {
    try {
        hodnota.value = eval(hodnota.value);
        vypocet = true;
    } catch (err) {
        hodnota.value = "Chyba";
    }
});

document.getElementById("smazPosledniZnak").addEventListener("click", function () {
    hodnota.value = hodnota.value.slice(0, -1);
    if (hodnota.value === "") hodnota.value = "0";
});

// Pridani polozky do vstupu
let tlacitka = document.querySelectorAll('.kalkulacka button');
tlacitka.forEach(function (tlacitko) {
    tlacitko.addEventListener('click', function () {
        let znak = tlacitko.innerHTML;
        let vyloucit = ["AC", "CE", "±", "="];
        if (!(vyloucit.includes(znak))) {
            if (hodnota.value === "0") hodnota.value = "";
            if (vypocet === true) {
                // ToDo: Nahradit regularnim vyrazem
                let cisla = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
                if ((cisla.includes(znak))) {
                    hodnota.value = "";
                }
                vypocet = false;
            }
            hodnota.value += znak;
        }
    });
});

// ToDo: Deleni nulou vraci nekonecno "infinity" místo "Chyba"

// ToDo: Nahradit
// ÷ /
// * +
// . ,

// ToDo: Omezit zadavani a vysledek na napr. 10 znaku

// ToDo: Znamenko ± nebo rozsirit nulu pres dve "pole"
function znamenko() {

}