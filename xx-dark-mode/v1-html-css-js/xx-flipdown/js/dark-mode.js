// Self-Invoking Function
(() => {
  // It doesn't wait for other things like images, subframes, and async scripts to finish loading.
  document.addEventListener('DOMContentLoaded', () => {

    const bodyClass = document.body.classList;
    const flipdownClass = document.querySelector('#flipdown').classList;
    const themeSwitcher = document.querySelector("#theme-switcher");
    const themeReset = document.querySelector("#theme-reset");
    const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");
    const currentTheme = localStorage.getItem("theme");

    // Set default theme
    if (currentTheme === "dark" || (!currentTheme && prefersDarkScheme.matches)) {
      bodyClass.add("theme-dark");

      flipdownClass.add('flipdown__theme-light');
      // Remove the default class flipdown__theme-dark
      flipdownClass.remove('flipdown__theme-dark');
    } else {
      bodyClass.add("theme-light");

      flipdownClass.add('flipdown__theme-dark'); // The default class flipdown__theme-dark is already set (you can remove this line)
    }

    // Toggle theme
    themeSwitcher.addEventListener("click", () => {
      bodyClass.toggle("theme-light");
      bodyClass.toggle("theme-dark");

      flipdownClass.toggle('flipdown__theme-dark');
      flipdownClass.toggle('flipdown__theme-light');

      theme = bodyClass.contains("theme-light") ? "light" : "dark";
      localStorage.setItem("theme", theme);
    });

    // Reset Theme
    themeReset.addEventListener("click", () => {
      bodyClass.remove("theme-light", "theme-dark");

      flipdownClass.remove('flipdown__theme-light');
      flipdownClass.add('flipdown__theme-dark');

      localStorage.removeItem("theme");
    })

  });
})();