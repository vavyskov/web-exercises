<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// var_dump($_POST);





// Configuration
$from = [
    'email' => 'user@vavyskov.cz',
    'name' => 'Jméno odesílatele',
    'password' => 'password',
];
$to = [
    'email' => 'foster.schiller5@ethereal.email', // Etc. https://ethereal.email/
    'name' => 'Jméno příjemce',
];

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load PHPMailer (install by composer)
// require 'vendor/autoload.php';

//Load PHPMailer (manual download)
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// Check for empty fields
if (
    empty($_POST['kategorie_udalosti'])
    || empty($_POST['informacni_system'])
    || empty($_POST['datum_vyskytu_udalosti'])
    || empty($_POST['nazev_stanice_dle_jmenne_konvence'])
    || empty($_POST['ip_adresa_stanice'])
    || empty($_POST['popis_udalosti'])
    // || empty($_POST['nahlasovatel_hodnost'])
    || empty($_POST['nahlasovatel_jmeno'])
    || empty($_POST['nahlasovatel_prijmeni'])
    || empty($_POST['nahlasovatel_utvar'])
    || empty($_POST['nahlasovatel_role'])
    || empty($_POST['nahlasovatel_telefon'])
    || empty($_POST['nahlasovatel_mobil'])
    || empty($_POST['nahlasovatel_email'])
    // || empty($_POST['spravce_hodnost'])
    || empty($_POST['spravce_jmeno'])
    || empty($_POST['spravce_prijmeni'])
    || empty($_POST['spravce_telefon'])
//    || empty($_POST['message'])
    // || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)
) {
    // Internal Server Error (form hacking, direct access to the URL etc.)
    http_response_code(500);
    exit();

    // Failure
//    readfile('failure.html');
//    exit();
}

// Strip HTML tags and convert special characters
$kategorie_udalosti = strip_tags(htmlspecialchars($_POST['kategorie_udalosti']));
$informacni_system = strip_tags(htmlspecialchars($_POST['informacni_system']));
$datum_vyskytu_udalosti = strip_tags(htmlspecialchars($_POST['datum_vyskytu_udalosti']));
$nazev_stanice_dle_jmenne_konvence = strip_tags(htmlspecialchars($_POST['nazev_stanice_dle_jmenne_konvence']));
$ip_adresa_stanice = strip_tags(htmlspecialchars($_POST['ip_adresa_stanice']));
$popis_udalosti = nl2br($_POST['popis_udalosti']);
$nahlasovatel_hodnost = strip_tags(htmlspecialchars($_POST['nahlasovatel_hodnost']));
$nahlasovatel_jmeno = strip_tags(htmlspecialchars($_POST['nahlasovatel_jmeno']));
$nahlasovatel_prijmeni = strip_tags(htmlspecialchars($_POST['nahlasovatel_prijmeni']));
$nahlasovatel_utvar = strip_tags(htmlspecialchars($_POST['nahlasovatel_utvar']));
$nahlasovatel_role = strip_tags(htmlspecialchars($_POST['nahlasovatel_role']));
$nahlasovatel_telefon = strip_tags(htmlspecialchars($_POST['nahlasovatel_telefon']));
$nahlasovatel_mobil = strip_tags(htmlspecialchars($_POST['nahlasovatel_mobil']));
$nahlasovatel_email = strip_tags(htmlspecialchars($_POST['nahlasovatel_email']));
$spravce_hodnost = strip_tags(htmlspecialchars($_POST['spravce_hodnost']));
$spravce_jmeno = strip_tags(htmlspecialchars($_POST['spravce_jmeno']));
$spravce_prijmeni = strip_tags(htmlspecialchars($_POST['spravce_prijmeni']));
$spravce_telefon = strip_tags(htmlspecialchars($_POST['spravce_telefon']));
//$message = nl2br($_POST['message']);

// Message
$subject = "Kontaktní formulář: $fname $lname";
//$content_text = "Odesílatel:\n - jméno: $fname $lname\n - e-mail: $email\n - telefon: $phone\n - datum narození: $dd.$mm.$yyyy\n - uživatelské jméno: $uname\n - heslo: $pword\n\nZpráva:\n$message";
$content_text = "Kategorie události: $kategorie_udalosti\n
Informační systém: $informacni_system\n
Datum výskytu události: $datum_vyskytu_udalosti\n
Název stanice dle jmenné konvece: $nazev_stanice_dle_jmenne_konvence\n
IP adresa stanice: $ip_adresa_stanice\n
\n
Popis události:\n
$popis_udalosti\n
\n
Nahlašovatel: $nahlasovatel_hodnost $nahlasovatel_jmeno $nahlasovatel_prijmeni\n
Útvar (dislokace) nahlašovatele: $nahlasovatel_utvar\n
Role nahlašovatele v systému: $nahlasovatel_role\n
Telefon nahlašovatele: $nahlasovatel_telefon\n
Služební mobil nahlašovatele: $nahlasovatel_mobil\n
E-mail nahlašovatele: $nahlasovatel_email\n
Správce IS: $spravce_hodnost $spravce_jmeno $spravce_prijmeni\n
Telefon správce IS: $spravce_telefon\n
";

$content_html = "
<p>Data:</p>
<ul>
    <li>Kategorie události: <b>$kategorie_udalosti</b></li>
    <li>Informační systém: <b>$informacni_system</b></li>
    <li>Datum výskytu události: <b>$datum_vyskytu_udalosti</b></li>
    <li>Název stanice dle jmenné konvece: <b>$nazev_stanice_dle_jmenne_konvence</b></li>
    <li>IP adresa stanice: <b>$ip_adresa_stanice</b></li>
    <li>Popis události: <b>$popis_udalosti</b></li>
    <li>Nahlašovatel: <b>$nahlasovatel_hodnost $nahlasovatel_jmeno $nahlasovatel_prijmeni</b></li>
    <li>Útvar (dislokace) nahlašovatele: <b>$nahlasovatel_utvar</b></li>
    <li>Role nahlašovatele v systému: <b>$nahlasovatel_role</b></li>
    <li>Telefon nahlašovatele: <b>$nahlasovatel_telefon</b></li>
    <li>Služební mobil nahlašovatele: <b>$nahlasovatel_mobil</b></li>
    <li>E-mail nahlašovatele: <b>$nahlasovatel_email</b></li>
    <li>Správce IS: <b>$spravce_hodnost $spravce_jmeno $spravce_prijmeni</b></li>
    <li>Telefon správce IS: <b>$spravce_telefon</b></li>
</ul>
";
/*$content_html = "
<p>Odesílatel:</p>
<ul>
    <li>jméno: <b>$fname $lname</b></li>
    <li>e-mail: <b>$email</b></li>
    <li>telefon: <b>$phone</b></li>
</ul>
<p>Zpráva:<br><b>$message</b></p>
";*/

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    // Character encoding
    $mail->CharSet = 'utf-8';

    // Localization
    $mail->setLanguage('cs', 'PHPMailer/language/');

    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_OFF;                         //Enable verbose debug output (DEBUG_OFF, DEBUG_CLIENT, DEBUG_SERVER, DEBUG_CONNECTION, DEBUG_LOWLEVEL)
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.vavyskov.cz';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $from['email'];                         //SMTP username
    $mail->Password   = $from['password'];                      //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable implicit TLS encryption
    $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom($from['email'], $from['name']);
    $mail->addAddress($to['email'], $to['name']);               //Add a recipient, name is optional
    //$mail->addAddress('ellen@example.com');                   //Add another a recipient, name is optional
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');            //Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');       //Optional name

    //Content
    $mail->isHTML(true);                                        //Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $content_html;
    $mail->AltBody = strip_tags($content_text);

    $mail->send();

    //echo 'Message has been sent.';

    // Success
    readfile('success.html');
} catch (Exception $e) {
    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    //echo $e->errorMessage();

    // Failure
    readfile('failure.html');
}

// Global exception
/* catch (\Exception $e)
{
    // PHP exception (note the backslash to select the global namespace Exception class).
    echo $e->getMessage();
} */
