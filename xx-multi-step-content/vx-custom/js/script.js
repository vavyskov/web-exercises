// (function() {
    "use strict";

    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Odeslat";
        } else {
            document.getElementById("nextBtn").innerHTML = "Další krok";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n);

        // Set progress
        setProgress();
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        // Required elements only
        y = x[currentTab].querySelectorAll("input[required], select[required], textarea[required]");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }



            // Native validation
            if (!y[i].checkValidity()) {
                // Přidání "nevalidní" třídy pro "grafické" účely
                y[i].className += " invalid";
                // Nastavení aktuálního stavu validace
                valid = false;

                // Informační výpis do terminálu (pouze pro účely ladění kódu)
                console.log("Prvek " + y[i].name + " je neplatný."); // ES5 verze
                // console.log(`Prvek ${y[i].name} je neplatný.`); // ES6 verze

                // Zobrazení "nativní validační" informace uživateli
                document.querySelector('#regForm input[type="submit"]').click();
            }



        }

        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
// })()





/**
 * Progress
 */
function setProgress() {
    // This function sets current step progress
    var progress = document.getElementById('progress');
    var progressBar = document.createElement('div');
    var x = document.getElementsByClassName('tab');
    var width = (currentTab + 1) / x.length * 100;

    progressBar.setAttribute('class', 'progress-bar');
    progressBar.style.width = width + '%';

    progress.innerText = "Krok " + (currentTab + 1) + " z " + x.length;
    progress.appendChild(progressBar);
}

/* Date placeholder */
var datumVyskytuUdalosti = document.getElementById('datum-vyskytu-udalosti');
datumVyskytuUdalosti.addEventListener("input", function (event) {
    if (event.target.value === '') {
        event.target.style.color = '#757575';
    } else {
        event.target.style.color = 'inherit';
    }
});

/* Select placeholder */
// var select = document.querySelector('select');
// select.addEventListener("input", function () {
//     if (select.value === '') {
//         select.style.color = '#757575';
//     } else {
//         select.style.color = 'inherit';
//     }
// });
var selects = document.querySelectorAll("select");
for (var i = 0; i < selects.length; i++) {
    selects[i].addEventListener("input", function (event) {
        if (event.target.value === '') {
            event.target.style.color = '#757575';
        } else {
            event.target.style.color = 'inherit';
        }
  });
}

/* Date calendar icon (fix browsers with "webkit engine" e. g. Google Chrome, Google Chromium, Microsoft Edge) */
var userAgent = navigator.userAgent;
if (/Chrome/.test(userAgent) || /Chromium/.test(userAgent) || /Edg/.test(userAgent)) {
    var dateIcon = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    dateIcon.setAttribute('viewBox', '0 0 448 512');
    dateIcon.setAttribute('class', 'date-icon');
    dateIcon.innerHTML = '<!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M152 24c0-13.3-10.7-24-24-24s-24 10.7-24 24V64H64C28.7 64 0 92.7 0 128v16 48V448c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V192 144 128c0-35.3-28.7-64-64-64H344V24c0-13.3-10.7-24-24-24s-24 10.7-24 24V64H152V24zM48 192H400V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16V192z"/>'
    // datumVyskytuUdalosti.parentNode.insertBefore(dateIcon, datumVyskytuUdalosti); // Insert before
    datumVyskytuUdalosti.parentNode.insertBefore(dateIcon, datumVyskytuUdalosti.nextSibling); // Insert after
    datumVyskytuUdalosti.addEventListener("input", function (event) {
        if (event.target.value === '') {
            event.target.parentNode.querySelector('.date-icon path').style.fill = '#757575';
        } else {
            event.target.parentNode.querySelector('.date-icon path').style.fill = 'inherit';
        }
    });
}

/* Stepper */
var i, x = document.getElementsByClassName("step");
for (i = 0; i < x.length; i++) {
    var child = document.createElement('span');
    child.innerHTML = '<span class="stepper-text">Krok</span> ' + (i + 1);
    x[i].appendChild(child);


    // Vytvoříme nový prvek, který bude sloužit jako obal
    // const wrapper = document.createElement('div');
    // if (x[i].classList.contains('active')) {
    //     wrapper.setAttribute('class', 'active');
    // }

    // Vložíme kopii původního prvku (včetně všech jeho potomků) dovnitř obalového prvku
    // wrapper.appendChild(x[i].cloneNode(true));

    // Nahradíme původní prvek obalovým prvkem (již obsahuje kopii původního prvku)
    // x[i].parentNode.replaceChild(wrapper, x[i]);
}
