"use strict";

/**
 * Období
 */ 
var obdobi = document.getElementById('range');
var aktualniRok = new Date().getFullYear();
obdobi.innerHTML = ', 2009&ndash;' + aktualniRok;

// document.getElementById('range').innerHTML = ', 2009&ndash;' + new Date().getFullYear();

/**
 * Ukazatel posunu
 */
var ukazatelPosunu = document.getElementById('scroll-progress');

function nastavUkazatelPosunu() {
    var vyskaOkna = window.innerHeight;
    var vyskaDokumentu = document.body.clientHeight;
    var posun = window.pageYOffset;
    var procentaPosunu = posun / (vyskaDokumentu - vyskaOkna) * 100;

    ukazatelPosunu.style.width = procentaPosunu + '%';
    
    //ukazatelPosunu.style.width = (window.scrollY / (document.body.clientHeight - window.innerHeight) * 100) + '%';
}

window.addEventListener('scroll', nastavUkazatelPosunu);

/**
 * Plynulý posun
 */
var links = document.querySelectorAll('a[href^="#"]');

for (var i = 0; i < links.length; i++) {
    links[i].addEventListener("click", plynulyPosun);
}

function plynulyPosun(event) {
    event.preventDefault();
    var href = this.getAttribute("href");

    window.scroll({
        top: document.querySelector(href).offsetTop,
        behavior: "smooth"
    });
}
