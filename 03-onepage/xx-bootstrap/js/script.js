"use strict";

/**
 * Období
 */
var obdobi = document.getElementById('range');
var aktualniRok = new Date().getFullYear();
obdobi.innerHTML = ', 2009&ndash;' + aktualniRok;

// document.getElementById('range').innerHTML = ', 2009&ndash;' + new Date().getFullYear();

/**
 * Ukazatel posunu
 */
var ukazatelPosunu = document.getElementById('scroll-progress');

function nastavUkazatelPosunu() {
    var vyskaOkna = window.innerHeight;
    var vyskaDokumentu = document.body.clientHeight;
    var posun = window.pageYOffset;
    var procentaPosunu = posun / (vyskaDokumentu - vyskaOkna) * 100;

    ukazatelPosunu.style.width = procentaPosunu + '%';

    //ukazatelPosunu.style.width = (window.scrollY / (document.body.clientHeight - window.innerHeight) * 100) + '%';
}

window.addEventListener('scroll', nastavUkazatelPosunu);
