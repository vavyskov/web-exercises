(function() {
    const cube = document.querySelector('.cube');

    const min = 1; // 1 ~ min jedno pootočení
    const max = 12; // 12 ~ max 3x dokola (4 stěny)

    function getRandom(min, max, multiple = 1) {
        // Nahodne číslo od-do krát volitelný násobek
        return (Math.floor(Math.random() * (max - min + 1)) + min) * multiple;
    }

    function rotate() {
        // náhodné číslo na konstce + mírné natočení kostky
        const xRand = getRandom(min, max, 90) + getRandom(-20, 20);
        const yRand = getRandom(min, max, 90) + getRandom(-20, 20);
        const zRand = getRandom(min, max, 90) + getRandom(-20, 20);

        cube.style.transform = `rotateX(${xRand}deg) rotateY(${yRand}deg) rotateZ(${zRand}deg)`;
    }

    cube.addEventListener('click', () => {
        rotate();
    })

    rotate();
})()
