# web-cube

Postup vytvoření:
1. HTML + CSS + JS (umět dočasně na stránce vypnout CSS a JS)
	1. vygenerovat náhodné číslo (používejte "technologie" k tomu, k čemu jsou určené)
		1. HTML - neumožňuje - nemá vestavěnou funkci na generování náhodných čísel (jedná se o statický jazyk, neumožňuje definovat "logiku")
		1. CSS - neumožňuje - nemá vestavěnou funkci na generování náhodných čísel "experimentálně to lze obejít pomocí pozastavení animace", ale zbytečně složitě a komplikovaně (ano pouze v preprocesorech tj. před zkompilováním do výsledného CSS)
			- https://uxdesign.cc/creating-randomness-with-pure-css-a990dafcd569 (karty)
			- https://dev.to/paulmercieca/create-random-cards-with-pure-css-1gdl (karty)
			- https://css-tricks.com/are-there-random-numbers-in-css/ (kostka)
		1. JS - "jediný" způsob na straně klienta :)
1. CSS: vytvořit rozložené strany (stěny) kostky ve 2D
	1. strany obsahují čísla
	1. čísla změnit na tečky (CSS content: '')
1. CSS: transformovat kostku do 3D
1. JS: volby
	1. počet kostek
	1. typ kostky (čísla tečky)
	1. barva/y
	1. průhlednost stěn

ToDo:
- for => forEach
- HTML cube structure to JS
- dokončit pootočení 1. kostky, aby byla trošku vidět horní a pravá strana
- na kostce tečky místo čísel
- hod by měl adekvátně změnit (nastavit) i přepínač první kostky
- náhodné mírné natočení kostky
- libovolný počet grafických kostek
- english variables and descriptions
- doplnit zvuk hození kostkou (kostkami)

Tips:
- vytvořit tečky pomocí "vícenásobného box-shadow" (zjednodušení)
	- čísla (2, 3) úhlopříčka začíná vlevo nahoře a končí vpravo dole
- 2D
    - https://codepen.io/Pyremell/pen/eZGGXX/
    - https://jsfiddle.net/estelle/6d5Z6/
    - https://github.com/tryton-vanmeer/React-Dice-Roller (React)
	- https://codewithchris.com/dice-roll-iphone-app/ (iOS App)
- 3D
    - CSS
        - https://codepen.io/jakob-e/pen/ebvxOW
        - https://www.adam-tyler.com/react-dice-complete/ (React)
    - canvas
        - https://www.webcodegeeks.com/html5/html5-3d-canvas-tutorial/
        - https://codesandbox.io/s/9eb1b
        - http://www.teall.info/2014/01/online-3d-dice-roller.html (zobrazuje **součet**)
- games
    - https://grrd01.github.io/Dice/
    - https://www.geeksforgeeks.org/building-a-dice-game-using-javascript/
    - https://www.golangprograms.com/dice-game-in-react-js.html
- true random
	- https://www.random.org/
