(function () {
    'use strict';

    // Initialization of Game object (class - first letter capital)
    var Hra = function(pocetkostek) {
        // Initialization of Cube object (class - first letter capital)
        var Kostka = function () {

            this.hazej = function () {
                // Generate number (1-6)
                this.hodnota = Math.floor((Math.random() * 6)) + 1;

                // Assign a number (0-5) to the text
                var face = ['front', 'top', 'left',  'right', 'bottom', 'back'];

                // Assign a CSS class
                this.trida = 'show-' + face[this.hodnota - 1];
            };

            // Initialize the default cube value
            this.hazej();
        };

        // Initialization of variables
        var kostky = [];

        // Create number of cubes
        for (var j = 0; j < pocetkostek; j++) {
            kostky.push(new Kostka());
        }

        // Generate a random number of every cube
        this.start = function() {

            // Generate cubes
            for (var i = 0; i < kostky.length; i++) {
                kostky[i].hazej();

                // Initialization of variables
                var graphicCube = [];
                var currentClass = null;
                var newClass = null;

                // Select graphic cube
                graphicCube[i] = document.querySelector('.cube-' + (i + 1));

                // Create new CSS class
                newClass = kostky[i].trida;

                // Change CSS class of graphic cube
                if ( graphicCube[i][currentClass] ) {
                    // Replace CSS class
                    graphicCube[i].classList.replace( graphicCube[i][currentClass], newClass );
                } else {
                    // Add new CSS class
                    graphicCube[i].classList.add( newClass );
                }

                // Set current CSS class
                graphicCube[i][currentClass] = newClass;

                // Value number (1-6) and Face text (array key 0-5)
                console.log(kostky[i].hodnota + " " + kostky[i].trida);
            }

            // Show separator
            console.log('---');
        };

        // Play button
        //this.playBtn = document.getElementById('play');
        ////this.playBtn.addEventListener('click', function () { hra.start(); }); // Long notation
        //this.playBtn.addEventListener('click', this.start);
    };

    // Create Game
    var hra = new Hra(3);

    // Initial startup
    //hra.start(); // Initial animation may not work correctly
    //requestAnimationFrame( hra.start() ); // Error - the function is started immediately
    //requestAnimationFrame(function () { hra.start(); }); // Long notation
    requestAnimationFrame(hra.start);





    //var cubeNumber = 3;

    // Name space VA
    window.va = {
        hra: hra
        //cubeNumber: cubeNumber,
        //message: "Hello",
        //osoba: {fname: "Homer"}
    };





    // Test Buttons
    var cube = document.querySelector('.cube');
    var radioGroup = document.querySelector('.radio-group');
    var currentClass = '';

    function changeSide() {
        var checkedRadio = radioGroup.querySelector(':checked');
        var newClass = 'show-' + checkedRadio.value;
        if ( currentClass ) {
            cube.classList.remove( currentClass );
        }
        cube.classList.add( newClass );
        currentClass = newClass;
    }
    // set initial side
    //changeSide();

    radioGroup.addEventListener( 'change', changeSide );



})();