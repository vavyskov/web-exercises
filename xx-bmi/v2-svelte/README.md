# svelte-bmi

## Creating a project

1. Create project:
    ```bash
    npm create svelte@latest .
    npm install
    git init && git add -A && git commit -m "Initial commit"
    ```

1. Configure [static adapter](https://kit.svelte.dev/docs/adapter-static).

## Developing

```bash
npm install
npm run dev
```
or
```bash
npm run dev -- --open
```

## Building

```bash
npm run build
npm run preview
```

## Notes

Links:
- https://github.com/svelte-add/svelte-add (postcss, bootstrap, imagetools)
    - `npx svelte-add@latest postcss`
    - (`npx svelte-add@latest bootstrap`)
- postcss (purgecss)
    - https://purgecss.com/getting-started.html
        - `npm i -D @fullhuman/postcss-purgecss`
    - (https://www.swyx.io/svelte_tailwind_setup)
- bootstrap
    - `sveltestrap`
    - `npm install bootstrap` (CSS)
- assets
    - https://pixabay.com/cs/vectors/anatomick%c3%a1-poloha-kostra-anatomie-6974512/
    - https://pixabay.com/cs/vectors/aura-t%c4%9blo-du%c5%a1e-sv%c4%9btlo-884270/

Git:
- Merge Selected (dev) into Current (test)


