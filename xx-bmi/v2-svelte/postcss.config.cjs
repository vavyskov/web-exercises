const autoprefixer = require('autoprefixer');
const purgecss = require('@fullhuman/postcss-purgecss')


const config = {
	plugins: [
		// purgecss({
		// 	content: [
		// 		"./src/**/*.svelte",
      	// 		// may also want to include HTML files
      	// 		"./src/**/*.html",
		// 		//'./**/*.html'
		// 	]
		// }),
		autoprefixer
	]
};

module.exports = config;
