import { sveltekit } from '@sveltejs/kit/vite';
import VitePluginBrowserSync from 'vite-plugin-browser-sync'


const config = {
	plugins: [
		sveltekit(),
		VitePluginBrowserSync()
	],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	}
};

export default config;
