# web-exercises

WEB Cvičení

Odkazy:
- [Ikony Font Awesome](https://fontawesome.com/search?m=free) (licence je uvedena přímo v SVG souboru)
- [Ikony Octicons](https://github.com/primer/octicons)
- [Typografie](https://prirucka.ujc.cas.cz/?id=810)
- [Kontrola kontrastu](https://contrastchecker.com/) - případně DevTools (Chrome, Firefox)

Možnosti plynulého posouvání:
- CSS
  - https://developer.mozilla.org/en-US/docs/Web/CSS/scroll-behavior
  - https://caniuse.com/css-scroll-behavior
- jQuery
  - https://jquery.com/browser-support/
  - https://caniuse.com/sr_jquery
- JS
  - Window.scroll()
    - https://developer.mozilla.org/en-US/docs/Web/API/Window/scroll
    - https://caniuse.com/mdn-api_window_scroll
- Polyfill
  - https://github.com/iamdustan/smoothscroll
    - https://cdnjs.com/libraries/iamdustan-smoothscroll/0.4.0
